package vn.mic.offshore.videox.orm.service;

public interface ServiceGenericAdapter<T> {
	T get(int id);
	Iterable<T> getAll();
	T save(T entity);
	Iterable<T> saveAll(Iterable<T> entities);
}
