package vn.mic.offshore.videox.orm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import vn.mic.offshore.videox.orm.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	User findByUserName(String name);
	
	@Query("SELECT u FROM User u WHERE u.firstName = :firstName and u.lastName = :lastName")
	List<User> findByName(@Param("firstName") String firstName, @Param("lastName") String lastName);
}
