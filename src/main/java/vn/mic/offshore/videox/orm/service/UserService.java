package vn.mic.offshore.videox.orm.service;

import vn.mic.offshore.videox.orm.entity.User;

public interface UserService extends ServiceGenericAdapter<User> {
	User getByUserName(String name);
}
