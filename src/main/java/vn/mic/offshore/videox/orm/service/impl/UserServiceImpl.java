package vn.mic.offshore.videox.orm.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.mic.offshore.videox.orm.entity.User;
import vn.mic.offshore.videox.orm.repository.UserRepository;
import vn.mic.offshore.videox.orm.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User get(int id) {
		Optional<User> user = this.userRepository.findById(id);
		return user == null ? null : user.get();
	}

	@Override
	public Iterable<User> getAll() {
		return this.userRepository.findAll();
	}

	@Override
	public User save(User entity) {
		return this.userRepository.save(entity);
	}

	@Override
	public Iterable<User> saveAll(Iterable<User> entities) {
		return this.userRepository.saveAll(entities);
	}

	@Override
	public User getByUserName(String name) {
		return this.userRepository.findByUserName(name);
	}

}
