package vn.mic.offshore.videox.administrator.authentication;

import javax.servlet.http.HttpSession;

public interface LoginManager {
	void putUser(HttpSession session);
	HttpSession getUser(String token);
}
