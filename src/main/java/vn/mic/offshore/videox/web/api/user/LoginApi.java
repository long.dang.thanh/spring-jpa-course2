package vn.mic.offshore.videox.web.api.user;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vn.mic.offshore.videox.orm.entity.User;
import vn.mic.offshore.videox.web.WebAdapter;
import vn.mic.offshore.videox.web.support.encrypt.BCrypt;

@RestController
public class LoginApi extends WebAdapter {
	
	@PostMapping("/login")
	public String login(@RequestParam(value = "userName") String userName, @RequestParam(value = "password") String password, HttpSession session) {
		User user = this.userService.getByUserName(userName);
		if (user == null) return "wrong user_name, please try again";
		if (!BCrypt.checkpw(password, user.getPassword()))
			return "wrong password, please try again";
		session.setAttribute("user", user);
		return "ok";
	}
	
	@PostMapping("/mobile-login")
	public String loginMobile(@RequestParam(value = "userName") String userName, @RequestParam(value = "password") String password, HttpSession session) {
		User user = this.userService.getByUserName(userName);
		if (user == null) return "wrong user_name, please try again";
		if (!BCrypt.checkpw(password, user.getPassword()))
			return "wrong password, please try again";
		session.setAttribute("user", user);
		return "ok";
	}
	
}
