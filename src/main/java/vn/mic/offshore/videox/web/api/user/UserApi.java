package vn.mic.offshore.videox.web.api.user;

import java.util.Date;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vn.mic.offshore.videox.orm.entity.User;
import vn.mic.offshore.videox.web.api.ApiAdapter;
import vn.mic.offshore.videox.web.support.encrypt.BCrypt;

@RestController
@RequestMapping(value = "/user")
public class UserApi extends ApiAdapter {
	@PostMapping("/register")
	public User register(@RequestParam(value = "userName", required = false) String userName, @RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "firstName", required = false) String firstName, @RequestParam(value = "lastName", required = false) String lastName, 
			@RequestParam(value = "phone", required = false) String phone, @RequestParam(value = "address", required = false) String address, 
			@RequestParam(value = "sex", required = false) Integer sex) {
		Date date = new Date();
		User user = new User();
		user.setAddress(address);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPhone(phone);
		user.setSex(sex);
		user.setUserName(userName);
		user.setPassword(BCrypt.hashpw(password));
		user.setInsertDatetime(date);
		user.setUpdateDatetime(date);
		return this.userService.save(user);
	}
}
