package vn.mic.offshore.videox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideoxApplication.class, args);
	}

}
